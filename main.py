#!/usr/bin/env python
# coding: utf-8

import os
import re
import subprocess

import pandas as pd


RAW_FOLDER = 'rawdata'


def get_raw_data():
    os.makedirs(RAW_FOLDER, exist_ok=True)
    url = 'ftp://ftp.ibge.gov.br/Trabalho_e_Rendimento/Pesquisa_Mensal_de_Emprego/Microdados_reponderados/PMEnova_2015.zip'
    subprocess.call('wget {0} -O {1}; unzip {1} -d {2}'.format(
        url, 'data.zip', RAW_FOLDER), shell=True)


def split_white(s):
    return [i.strip() for i in re.split("\t|(\s\s\s+)", s) if i]


def load_legenda():
    with open('INPUT.txt', "r", encoding="latin-1") as f:
        inp = [i for i in f.read().splitlines()[4:] if len(i) > 1]

    ls = [[j for j in split_white(i) if j] for i in inp]
    return [{
        "start": int(l[0][1:]),
        "var": l[1],
        "size": l[2],
        "name": " ".join(l[3:]).replace("/*", "").replace("*/", "").strip(),
        } for l in ls]


def load_data(legenda):
    with open(os.path.join(RAW_FOLDER, 'PMEnova.012015.txt'), "r") as f:
        inp = f.read().splitlines()

    proc = []
    for l in inp[:10]:  # LIMIT!!!!! Big file
        dicio = {}
        for i, v in enumerate(legenda):
            start = v["start"]
            try:
                end = legenda[i+1]["start"]
            except IndexError:
                end = len(l)+1
            dicio[v["name"]] = l[start-1:end-1]
        proc.append(dicio)
    return proc


legenda = load_legenda()
load_data(legenda)

df = pd.read_excel('dicionario.xls')
df.columns = ['start', 'size', 'code', 'name', 'type', 'description']
df.code = df.code.ffill()
